﻿$(function () {


    // отправка сообщения на сервер
    $('#send').click(function () {
        send();
    });

    // блокировка юзера
    $('#block').click(function () {
        block();
    });

    //получение списка сообщений при запуске чата 
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: '/Home/GetAllMessages',
        success: function (result) {
            if (result != null) {
                var length = result.messageList.length;
                for (var i = 0; i < length; i++) {
                    var id = result.messageList[i].Id;
                    var text = result.messageList[i].Text;
                    var authorName = result.messageList[i].AuthorName;

                    text = smiliesAndLinksTransform(text);

                    $('#chat').append('<div class="post" value="' + id + '"> <div id="authorname">' + authorName + '</div> <div class="text">' + text + '</div></div>');
                    $('#Id').val(id);
                }
                var lastPostPosition = $('.post').last().position();
                if (lastPostPosition != null) {
                    $('#chat').animate({ scrollTop: $(lastPostPosition).top }, 10);
                }
            }
        }
    });


    // Опрос сервера на наличие новых сообщений
    setInterval(function () {
        var lastMessageId = $('#Id').val();
        var userName = $('#username').val();
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            url: '/Home/GetLatestMessage',
            data: JSON.stringify({ id: lastMessageId, userName: userName }),
            success: function (result) {
                if (result != null) {

                    if (!result.needreload) {
                        if (result.text != null) {


                            var text = result.text;
                            var authorName = result.authorName;
                            var id = result.idLatest;

                            text = smiliesAndLinksTransform(text);
                            $('#chat').append('<div class="post" value="' + result.idLatest + '"> <div id="authorname">' + authorName + '</div> <div class="text">' + text + '</div></div>');
                            $('#chat').animate({ scrollTop: 50 * id }, 500);
                            $('#Id').val(id);
                        }
                    } else {
                        location.reload(true);
                    }
                }
            }
        });
    }, 1500);

    var userDeleted = false;
    var userBlocked = false;
    // Проверка заблокирован ли пользователь
    setInterval(function () {
        var userName = $('#username').val();
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            url: '/Home/IsBlocked',
            data: JSON.stringify({ userName: userName }),
            success: function (result) {
                if (result != null) {
                    if (result.isBlocked) {
                        $('#send').attr('disabled', true);
                        $('#message').attr('disabled', true);
                        keyboardBind(false);
                        if (!result.isBlockedForever && !userBlocked) {
                            alert('Вы были заблокированы до ' + result.unblockTime + '. Чат доступен в режиме readonly');
                            userBlocked = true;
                        }
                        if (result.isBlockedForever && !userDeleted) {
                            userDeleted = true;
                            closeWindow();
                        }
                    } else {
                        if ($('#send').attr('disabled')) {
                            $('#send').attr('disabled', false);
                            $('#message').attr('disabled', false);
                            keyboardBind(true);
                            userBlocked = false;
                        }
                    }
                }
            }
        });
    }, 1000);


    keyboardBind(true);

    // Получение списка он-лайн пользователей
    setInterval(function () {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            url: '/Home/GetUsersOnlineList',
            success: function (result) {
                var tempstr = '';
                for (var i = 0; i < result.toUsersOnline.length; i++) {
                    tempstr += result.toUsersOnline[i] + '\r';
                }
                $('#usersonline').val(tempstr);
            }
        });
    }, 1000);
});


function send() {
    var typedmessage = $('#message').val();
    var idmessageToEdit = $('#idmessagetoedit').val() * 1;
    if (typedmessage != '' || idmessageToEdit != 0) {
        $.ajax(
            {
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                url: '/Home/SendMessage',
                data: JSON.stringify({ id: idmessageToEdit, message: typedmessage }),
                success: function() {
                    $('#message').val('');
                    $('#idmessagetoedit').val();
                    if (idmessageToEdit != 0) {
                        location.reload(true);
                    }
                }
            });
    }
}
    

function edit() {
        var userName = $('#username').val();
        var posts = $('#chat').children();
        var length = posts.length - 1;
        var messageId = 0;
        
        for (var i = length; i >= 0; i--) {
            if (userName == $(posts[i]).children('#authorname').text()) {
                messageId = $(posts[i]).attr('value')*1;
                $('#idmessagetoedit').val(messageId);
                break;
            }
        }
        if (messageId != 0) {
            $.ajax(
                {
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json',
                    url: '/Home/GetMessageToEdit',
                    data: JSON.stringify({ id: messageId }),
                    success: function (result) {
                        if (result != null) {
                            $('#message').val(result.text);
                        }
                    }
                });
        }

    }//

    // подписка на / отключение  события(-ий) клавиатуры (enter, uparrow) в поле ввода сообщения
    function keyboardBind(flag) {
        if (flag) {
            $('#message').bind('keydown', function (e) {
                e = e || window.event;
                if (e.keyCode == 38) {
                    edit();
                }
                if (e.keyCode == 13) {
                    send();
                }
            });
        } else {
            $('#message').unbind();
        }
    }

    function block() {
        if ($('#usertoblock') != null) {
            var userToBlock = $('#usertoblock').val();
            var timeToBlock = $('#timetoblock').val();
            $.ajax(
                {
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json',
                    url: '/Home/BlockUser',
                    data: JSON.stringify({ userName: userToBlock, blockTime: timeToBlock }),
                    success: function (result) {
                        if (result.userExists && !result.isAdmin) {
                                alert('Пользователь с ником ' + userToBlock + ' успешно заблокирован на ' + timeToBlock + ' минут');                         
                        } else {
                            if (result.isAdmin) {
                                alert('Пользователь с ником ' + userToBlock + ' является администратором сайта и не может быть заблокирован');
                            } else {
                                alert('Пользователя с ником ' + userToBlock + ' не существует или он не может быть заблокирован');
                            }
                        }
                    }
                });
        }
    }


    function smiliesAndLinksTransform(text) {
        
        while (text.indexOf(':)') + 1) {
            text = text.replace(':)', '<img src="../../Content/images/img-smile-01.png" />');
        }

        while (text.indexOf(':(') + 1) {
            text = text.replace(':(', '<img src="../../Content/images/img-smile-02.png" />');
        }

        while (text.indexOf(';)') + 1) {
            text = text.replace(';)', '<img src="../../Content/images/img-smile-03.png" />');
        }

        text = text.replace( /http\:\/\/(\S+[\w\-\/])/ig , "<a href='http://$1' target=\"_blank\">" + text + "</a>");
        text = text.replace(/https\:\/\/(\S+[\w\-\/])/ig, "<a href='http://$1' target=\"_blank\">" + text + "</a>");
        text = text.replace(/www.\/\/(\S+[\w\-\/])/ig, "<a href='http://$1' target=\"_blank\">" + text + "</a>");

        return text;
    }


    function closeWindow() {
        alert('Вы были удалены из чата');
        var win = window.open('', '_self');
        win.close();
    }


   