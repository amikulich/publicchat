﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PublicChat.Core;

namespace PublicChat.Functional.Service
{
    public class MessageService: IMessageService
    {
        public Message GetMessageById(int id)
        {
            return ApplicationManager.NHibernateSession.Get<Message>(id);
        }

        /// <summary>
        /// Получение первого сообщения из БД
        /// </summary>
        /// <returns></returns>
        public Message GetFirstMessage()
        {
            return ApplicationManager.NHibernateSession.CreateCriteria<Message>().List<Message>().FirstOrDefault();
        }
        /// <summary>
        /// Получение последнего сообщения из БД
        /// </summary>
        /// <returns></returns>
        public Message GetLastMessage()
        {
            return ApplicationManager.NHibernateSession.CreateCriteria<Message>().List<Message>().LastOrDefault();
        }
        
        /// <summary>
        /// Получение списка сообщений из БД
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Message> GetAllMessages()
        {
            return ApplicationManager.NHibernateSession.CreateCriteria<Message>().List<Message>();
        }

        public void SaveMessage(Message message)
        {
            ApplicationManager.OpenDataTransaction();
            ApplicationManager.NHibernateSession.Save(message);
            ApplicationManager.CloseDataTransaction();
        }

        public Message GetSomeMessage(IDataReader dr)
        {
            var message = new Message {AuthorName = dr["AuthorName"].ToString(), Id = Convert.ToInt32(dr["Id"]), Text = dr["Text"].ToString()};
            return message;
        }

    }
}
